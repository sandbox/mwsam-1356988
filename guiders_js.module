<?php
define('GUIDERS_SETTINGS_PATH', 'admin/content/guiders/packs');

/**
 * @file
 * CRUD UI interface for the Guiders-JS module
 */

/**
 * Implements hook_init().
 */
function guiders_js_init() {
  if (!user_access('access guiders')) {
    return;
  }
  // Getting the relevant guider's pack
  // Handling frontpage token replacemnet
  $path = drupal_is_front_page() ? '<front>' : check_url($_GET['q']);

  // Getting the guider's pack that should be displayed in our current path
  $result = db_query("SELECT * FROM {guiders_packs} WHERE path = '%s'", $path);
  $gp = db_fetch_object($result);

  if (!empty($gp)) {
    // Adding the required library's files
    $lib_path = libraries_get_path('guiders-js');
    drupal_add_js($lib_path . '/guiders.js');
    drupal_add_css($lib_path . '/guiders.css');

    // Generating the cache id
    $cid = 'guiders-pack-' . $gp->gpid;

    // Checking if the data exists in the cache
    $cached_js = cache_get($cid, 'cache_guiders');

    // Creating the js because we couldn't find the cached version
    if (empty($cached_js)) {
      // Getting the pack's guiders
      $result = db_query("SELECT * FROM {guiders} WHERE gpid = %d", $gp->gpid);

      // Get the raw data into the guiders array
      $guiders = array();
      while ($guider = db_fetch_object($result)) {
        $guiders[] = !empty($guider->data) ? unserialize($guider->data) : new stdClass();
      }

      // Creating the js and attaching the library
      if (count($guiders) > 0) {
        $parsed_js = _guiders_js_parse_guiders($guiders);

        // Setting the data to the cache
        cache_set($cid, $parsed_js, 'cache_guiders', CACHE_TEMPORARY);
      }
    }
    else {
      // Getting the js from the cached data
      $parsed_js = $cached_js->data;
    }

    // Adding the generated script to the page
    drupal_add_js($parsed_js, 'inline', 'footer');
  }
}

/**
 * Implements hook_menu().
 */
function guiders_js_menu() {
  $items = array();
  $base = array(
    'access arguments' => array('administer guiders'),
    'file' => 'guiders_js.admin.inc',
  );

  $items[GUIDERS_SETTINGS_PATH] = array(
    'title' => 'Guiders',
    'description' => 'Add, edit and remove guiders.',
    'page callback' => 'guiders_js_packs_list_page',
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/list'] = array(
    'title' => 'Guider\'s Packs List',
    'page callback' => 'guiders_js_packs_list_page',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -11,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/add'] = array(
    'title' => 'Add Guider Pack',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_packs_add_form'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -10,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/%guiders_js_packs'] = array(
    'title' => 'List Guiders Pack',
    'page callback' => 'guiders_js_list_page',
    'page arguments' => array(4),
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/%guiders_js_packs/list'] = array(
    'title' => 'List',
    'page callback' => 'guiders_js_list_page',
    'page arguments' => array(4),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -11,
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/%guiders_js_packs/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_packs_add_form', 4),
    'type' => MENU_CALLBACK,
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/%guiders_js_packs/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_delete_confirm', 4),
    'type' => MENU_CALLBACK,
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/%guiders_js_packs/add'] = array(
    'title' => 'Add Guider',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_add_form', 4),
    'type' => MENU_LOCAL_TASK,
    'weight' => -10,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/%guiders_js_packs/%guiders_js/edit'] = array(
    'title' => 'Edit Guider',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_add_form', 4, 5),
    'type' => MENU_CALLBACK,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/%guiders_js_packs/%guiders_js/delete'] = array(
    'title' => 'Delete Guider',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_delete_confirm', 4, 5),
    'type' => MENU_CALLBACK,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/path/autocomplete'] = array(
    'title' => 'Guider Pack Path Autocomplete',
    'page callback' => 'guiders_js_path_autocomplete',
    'type' => MENU_CALLBACK,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/path/add_more'] = array(
    'title' => 'Guider Pack Path Add More',
    'page callback' => 'guiders_js_add_more_callback',
    'type' => MENU_CALLBACK,
  ) + $base;

  return $items;
}

/**
 * Implements hook_perm().
 */
function guiders_js_perm() {
  return array(
    'administer guiders',
    'access guiders',
  );
}

/**
 * Implements hook_flush_caches().
 */
function guiders_js_flush_caches() {
  return array('cache_guiders');
}

/**
 * Loads a guiders pack by it's unique id
 */
function guiders_js_packs_load($gpid) {
  $result = db_query("SELECT * FROM {guiders_packs} WHERE gpid = %d", $gpid);
  return db_fetch_object($result);
}

/**
 * Loads a guiders by it's unique id
 */
function guiders_js_load($gid) {
  $result = db_query("SELECT * FROM {guiders} WHERE gid = %d", $gid);
  return db_fetch_object($result);
}

/**
 * Gets data from the guiders or guiders_packs tables.
 *
 * A genric wrapper for querying data.
 *
 * @param $table
 *   A string containing a table name.
 *
 * @param $fields
 *   An associative array containing fields' names and values to set as conditions
 *   to the query.
 *
 * @return
 *   Query object with the results of the query.
 */
function _guiders_js_get($table, $fields = array()) {
  $schema = drupal_get_schema($table);
  $wheres = array();
  $args = array();
  foreach ($fields as $field_name => $field_val) {
    $type = $schema['fields'][$field_name]['type'];
    $wheres[] = "$field_name = " . db_placeholders(array($field_val), $type);
    $args[] = $field_val;
  }
  $where = $wheres ? " WHERE " . implode(' AND ', $wheres) : '';
  return db_query("SELECT * FROM {" . $table . "}$where", $args);
}

/**
 * Sets data to the guiders or guiders_packs tables.
 *
 * A genric wrapper for setting data.
 *
 * @param $table
 *   A string containing a table name.
 *
 * @param $key
 *   A string containing a priamry key.
 *
 * @param $fields
 *   An associative array containing fields' names and values to set as conditions
 *   to the query.
 *
 * @return
 *   Query object with the results of the query.
 */
function _guiders_js_set($table, $key, $fields) {
  $schema = drupal_get_schema($table);
  $wheres = array();
  $args = array();
  foreach ($key as $key_name => $key_val) {
    $type = $schema['fields'][$key_name]['type'];
    $wheres[] = "$key_name = " . db_placeholders(array($key_val), $type);
    $args[] = $key_val;
  }
  $result = db_query("SELECT 1 FROM {" . $table . "} WHERE " . implode(' AND ', $wheres), $args);

  $set = array();
  $values = array();
  foreach ($fields as $field => $value) {
    $type = $schema['fields'][$field]['type'];
    $set[] = "$field = " . db_placeholders(array($value), $type);
    $values[] = $value;
  }
  if ((bool) db_result($result)) {
    // Update.
    return db_query("UPDATE {" . $table . "} SET " . implode(', ', $set) . " WHERE " . implode(' AND ', $wheres), array_merge($values, $args));
  }
  else {
    // Insert. Must also set the values of keys.
    return db_query("INSERT INTO {" . $table . "} SET " . implode(', ', array_merge($wheres, $set)), array_merge($args, $values));
  }
}

/**
 * Deletes data from the guiders or guiders_packs tables.
 *
 * A genric wrapper for deleting data.
 *
 * @param $table
 *   A string containing a table name.
 *
 * @param $fields
 *   An associative array containing fields' names and values to set as conditions
 *   to the query.
 *
 * @return
 *   Query object with the results of the query.
 */
function _guiders_js_del($table, $fields = array()) {
  $schema = drupal_get_schema($table);
  $wheres = array();
  $args = array();
  foreach ($fields as $field => $value) {
    $type = $schema['fields'][$field]['type'];
    $wheres[] = "$field = " . db_placeholders(array($value), $type);
    $args[] = $value;
  }
  $where = $wheres ? " WHERE " . implode(' AND ', $wheres) : '';
  return db_query("DELETE FROM {" . $table . "}$where", $args);
}

/**
 * Dynamically creates Guiders-JS code
 */
function _guiders_js_parse_guiders($guiders) {
  $guiders_js = '';
  $size = count($guiders);
  for ($i = 0; $i < $size; $i++) {
    $guider = $guiders[$i];

    // Building the buttons json array
    $buttons = '[';
    foreach ($guider['buttons_fs'] as $button) {
      if (isset($button['class']) || isset($button['onclick'])) {
        $button['name'] = check_plain($button['name']);
        $button['other'] = check_plain($button['other']);
        $button['class'] = check_plain($button['class']);
        $button['onclick'] = check_plain($button['onclick']);
      }
      $buttons .= json_encode($button) . ',';
    }
    $buttons .= ']';

    // Sanitize the description field
    $desc = check_markup($guider['desc'], $guider['desc_format']);

    // Making the desc field be one liner
    $desc = preg_replace("/\r?\n/", "<br />", $desc);

    // Appending the full js structure
    $guiders_js .= 'guiders.createGuider({'
      . 'id: "guider_' . $i . '",'
      . 'title: "' . check_plain($guider['title']) . '",'
      . 'description: "' . $desc . '",'
      . 'buttons: ' . ($buttons == "[]" ? '[{"name": "Next"}, {"name": "Close"}]' : $buttons) . ','
      . 'next: "guider_' . ($i + 1) . '", '
      . 'overlay: ' . ($guider['overlay'] == 1 ? 'true' : 'false') . ','
      . 'xButton: ' . ($guider['close'] == 1 ? 'true' : 'false') . ','
      . (!empty($guider['attach']) ? 'attachTo: "' . check_plain($guider['attach']) . '",' : '')
      . (!empty($guider['attach']) && !empty($guider['position']) ? 'position: ' . check_plain($guider['position']) . ',' : '')
      . 'width: "' . check_plain($guider['width']) . '", '
    . '})';

    // Appending "show()" to the first item to start the chain automatically
    if ($i == 0) {
      $guiders_js .= '.show();';
    }
    else {
      $guiders_js .= ';';
    }
  }

  return $guiders_js;
}
